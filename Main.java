import java.sql.*;

/* STEROWNIK DO POŁĄCZENIA JDBC oraz krótki przykład wykorzystania - 

https://www.javatpoint.com/example-to-connect-to-the-mysql-database

*/

public class Main {
	


	public static void main (String[] args) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection connection = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/przepisy_java?useUnicode=true&characterEncoding=utf-8","root","");
			Statement st = connection.createStatement();
//			st.execute("SET NAMES 'UTF8';");
//			st.execute("USE `przepisy_java`;");
//			st.execute("SET CHARACTER SET 'UTF8';");
			ResultSet rs = st.executeQuery("SELECT * FROM `skladniki`;");
			while (rs.next()) {
				System.out.println("Kolumna z nazwą zawiera: "+ rs.getString(2) +
						", natomiast opis to: " + rs.getString(3));
				//type type = (type) .nextElement();
				
			}
			st.execute("INSERT INTO `galerie` (`nazwa`) VALUES ('Kopytka'), ('Gołąbki'), "
					+ "('Pomidorowa'), ('Schabowe w siemu lnianym');");
			connection.close();
		} catch (SQLException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		
		
		
		
		
//		try {
//			Class.forName("com.mysql.jdbc.Driver");
//			Connection c = DriverManager.getConnection("jdbc:mysql://localhost:3306/przepisy_java?useUnicode=true&characterEncoding=utf-8","root","");
//			Statement st = c.createStatement();
////			st.execute("INSERT INTO `skladniki` (`nazwa`,`opis`) VALUES ('czosnek', 'Nadaje niepowtarzalny aromat'), ('Pomidor', 'Dobry do sałatek i sosów'),"
////					+ " ('Papryka', 'Świeża przydaje się do niemal każdego gulaszu');");
////			c.close();
//			ResultSet rs = st.executeQuery("SELECT * FROM `skladniki`;");
//			
//			
//			while(rs.next()) {
//				
//				System.out.println(rs.getString(2) + ", którego właściwości to " + rs.getString(3));
//			}
//			
//			c.close();
//		} catch (SQLException | ClassNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
}
