package Przepis;

import java.util.List;
import java.util.Vector;

import Przepis.Skladniki.Val;

/* Klasa stanowi adaptację przepisów z bazy danych przepisy.sql
 * Pozwala na przechowywanie jednego obiektu - przepisu. Klasa może stanowić 
 * podstawę do stworzenia listy przepisów (zarówno poprzez odpowiednią klasę - 
 * zbiory jak i zwykłą tablicę. Klasa powinna pozwalać na zapis przechowywanych 
 * wartości do bazy danych (oraz odczyt danych z bazy danych). 
 */

public class Przepisy {
	//poniżej tzw. pola prywatne. Pola te nie mogą być w żaden sposób odczytane przez
	//inne klasy programu ani żadne klasy pochodne (extends). JEDYNYM SPOSOBEM ODCZYTU 
	//jest utworzenie ODPOWIEDNICH PUBLICZNYCH METOD (funkcji), które np. będą pobierać 
	//lub ustawiać odpowiednie wartości do tych zmiennych
	//ukrywane (robienie prywatnych) elementów klasy jest powszechne i chroni:
	// - przed przypadkową modyfikacją danych we wskzanym polu/przed przypadkowym wywałaniem metody
	// - zmniejsza ilość wyświetlanych składowych klasy - jeżeli klasę dajemy komuś (np. do dalszego
	// użytku) to osoba nie będzie widzieć wszystkich składowych pomocniczych lub 
	// sprawdzających, a będzie widzieć jedynie te elementy, które w danej chwili będą naprawdę potrzebne
	// - przed zapisem nieprawidłowych danych do pola. 
	
	// Ogólnie nie byłoby wskazane by ktokolwiek modyfikował nam w dowolny sposób konto w banku (ilość
	// gotówki na koncie). Możemy dziennie wydać jedynie określoną kwotę, przy odpowiednim zabezpieczeniu
	// transakcji (np. PIN, hasło głosowe itp.). Dodatkowo kolejnym kryterium jest ilość funduszy na samym
	// koncie (nie kupimy samochodu za 200.000 gdy na koncie mamy 20). 
	
	// W przypadu ukrycia składkowych najlepszym przykładem może być komputer, który włączamy poprzez 
	// przycisk, możemy podłączyć urzędzenia poprzez odpowiednie interfejsy oraz obsługujemy go dzięki
	// posiadaniu odpowiedniego oprogramowania. Nie interesuje nas w jaki sposób styki łączą wejście 
	// USB z chipsetem, a ten z procesorem. Nie iteresuje nas w jaki sposób oprogramowanie odwołuje się
	// do poszczególnych elementów elektronicznych komputera - ma po prostu wyświetlać grafikę, wyonywać
	// odpowiednie obliczenia i zapewniać rozrywkę oraz być narzędziem pracy. Elementy techniczne to
	// skadowe prywatne, które działają poza naszą jurysdykcją. 
	
	//UWAGA! Składowe prywatne mogą być poprzedzane słowem private (wszystkie pola bez słowa są trkatowane jako prywatne)
	String nazwa;
	String wykonanie;
	String opis;
	Trudnosc poziom;
	int czas; //podawany w pelnych minutach
	// poniżej przykład obiektu List. List jest tzw. interfejsem czyli klasą czysto wirtualną. Oznacza to,
	// że jako taka nie może stać się obiektem, może jednak doskonale opisywać co najmniej jeden rzeczywisty
	// obiekt oraz obiekt tworzony z klasy, która będzie utworzona na podstawie wspomnianego interfejsu.
	
	//Interfejs to specjalny typ złożony, w którym definiuje się wszystkie składowe, którę BĘDĄ MUSIAŁY BYĆ
	//zaimplementowane w każdym nowym elemencie (klasie bądz innym interfejsie). Dzięki temu podczas wywoływania
	//obiektu dziedziczącego po interfejsie mamy pewność, że musi on posiadać każdą w ten sposób zdefiniowaną 
	//składową. Przykładowo jeden pilot może służyć do obsługi telewizora, drugi do klimatyzacji, trzeci do
	//projektora, a czwrty do zabawki elektornicznej. Każdy z nich będzie miał strzałki lewo/prawo/góra/dół i każdy
	//z nich będzie miał przycisk włącz/wyłącz. W związku z tym każdy z nich może być utwrzony na podstawie
	//interfejsu PostawowePrzyciski i kązdy z nich będzie miał do nich przypisaną odpowiednią funkcjonlaność - 
	// włącz i wyłącz wszystkie taką samą, jednak strzałki w zabawce będą służyy do rochu, podczas gdy w telewizorze
	// można będzie się dzięki nim poruszać po menu.
	List<Skladniki> sklad;
	//Skladniki[] sklad;
	
	
	public static enum Trudnosc {
		LATWY(0),
		SREDNIOLATWY(1),
		SREDNI(2),
		SREDNIOTRUDNY(3),
		WYMAGAJACY(4),
		TRUDNY(5),
		BARDZOTRUDNY(6);
		
		private final int v;
		
		private Trudnosc(int v) {this.v=v;}
		
		public int get() {return v;}
	}
	
	//poniżej przykład tzw. przeciążania parametrów funkcji (w tym wypadku samego konstruktora)
	//przeciążanie polega na utworzeniu co najmniej dwóch funkcji o takie samej nazwie jednak 
	//przyjmujących zupełnie inne parmetry wejściowe. Parametry mogą się różnić od siebie typami
	//(nawet muszą); poszczególne funkcje mogą także przyjmować je w różnej ilości. W języku Java
	//nie ma możliwości nadać domyślnej wartości poszczególnym parametrom wejściowym dlatego też
	//poniższe konstruktory wywołują kolejne, swoje bardziej rozbudowane wersje.
	//Przekazują odpiwiednie wartości do najbardziej rozbudowanej wersji, która nadaje wszystkich polom
	//odpowiednie wartości.
	public Przepisy() {
		this("","","",0,0);
	}
	
	public Przepisy(String nazwa) {
		this(nazwa,"","",0,0);
	}
	
	public Przepisy(String nazwa, int poziom) {
		this(nazwa,"","",poziom,0);
	}
	
	public Przepisy(String nazwa, Trudnosc poziom) {
		this(nazwa,"","",poziom,0);
	}
	
	public Przepisy(String nazwa, String wykonanie, String opis) {
		this(nazwa,wykonanie,opis,0,0);	
	}
	
	public Przepisy(String nazwa, String wykonanie, String opis, int poziom) {
		this(nazwa,wykonanie,opis,getPoziom(poziom),0);	
	}
	//tutaj mamy przypadek konstruktora, który może wyrzucać wyjątki (ogólne Exception). Dzieje się tak
	//przez użycie w nim metody getCzas(czas), która wyrzuca wyjątek w przypadku niepowodzenia
	//tego typu konstruktor trzeba wywoływac w klauzuli try{} catch() {}
	public Przepisy(String nazwa, String wykonanie, String opis, Trudnosc poziom, String czas) throws Exception {
		this(nazwa,wykonanie,opis,poziom,getCzas(czas));	
	}
	
	public Przepisy(String nazwa, String wykonanie, String opis, int poziom, String czas) throws Exception {
		this(nazwa,wykonanie,opis,getPoziom(poziom),getCzas(czas));	
	}
	
	public Przepisy(String nazwa, String wykonanie, String opis, int poziom, int czas) {
		this(nazwa,wykonanie,opis,getPoziom(poziom),czas);	
	}
	
	public Przepisy(String nazwa, String wykonanie, String opis, Trudnosc poziom, int czas) {
		this.nazwa=nazwa;
		this.wykonanie=wykonanie;
		this.opis=opis;
		this.poziom=poziom;
		this.czas=czas;
		sklad = new Vector<>();
	}
	
	//publiczne metody pozwalające na wyświetlenie poszczególnych pól i ich zawartości.
	public String getNazwa() {return nazwa;}
	public String getWykonanie() {return wykonanie;}
	public String getOpis() {return opis;}
	//metoda pokazuje możliwości stosowania metod pobierającyh dane; zamast wyświetlić 
	//dane enumeryczne, dzięki odpowiedniemu zastosowaniu konstrukcji switch...case
	//możemy wyświetlić odpowiednio nazwany poziom trudności przepisu
	public String getPoziom() {
		String r="Domyślny";
		switch(poziom.get()) {
		case 0: r="łatwy"; break;
		case 1: r="średniołatwy"; break;
		case 2: r="średni"; break;
		case 3: r="średniotrudny"; break;
		case 4: r="wymagający"; break;
		case 5: r="trudny"; break;
		case 6: r="bardzo trudny"; break;
		default: r="Brak skali!";
		}
		return r;
	}
	//funkcja ma za zadanie tworzyć ciąg znakowy wypisujący czas, w jakim da się przygotować opisywane danie
	//funkcja zwraca ciągi znakowe, np. 1 godzina 39 minut, 45 minut, itp. 
	//UWAGA! Funkcja nie zawsze działa poprawnie, należy ją naprawić!
	public String getCzas() {
		String r="";
		if (czas>59) {
			int h = czas/60;
			int m = czas%60;
			r=String.valueOf(h);
			if (h==1) r+=" godzina";
			else r+=" godzin";
			if ((h<10 || h>20) && (h%10<5 && h%10>1)) r+= "y";
			//TODO: naprawić jeżeli jest 0 zminut należy poniższe warunki pominąć (nie wyświetlać minut)
			r+=" i " + m;
			if (m==1) r+=" minuta";
			r+=" minut";
			if ((m<10 || m>20) && (m%10<5 && m%10>1)) r+= "y";
		}
		//TODO: trzeba dodać wyświetlanie czasu gdy przepis zajmuje mniej niż 60 minut
		return r;
	}
	
	//metoda prywatna, do celu wewnątrzobiektowego; zwraca enumeryczny typ poziomu przepisu
	private static Trudnosc getPoziom(int poziom) {
		
		switch(poziom) {
		case 0: return Trudnosc.LATWY; 
		case 1: return Trudnosc.SREDNIOLATWY; 
		case 2: return Trudnosc.SREDNI;
		case 3: return Trudnosc.SREDNIOTRUDNY; 
		case 4: return Trudnosc.WYMAGAJACY; 
		case 5: return Trudnosc.TRUDNY;
		case 6: return Trudnosc.BARDZOTRUDNY;
		default: return Trudnosc.WYMAGAJACY;
		}
		//return Trudnosc.WYMAGAJACY;
	}
	
	//publiczne metody mające na celu ustawienie odpowiednich wartości dla pól prywatnych
	public void setNazwa(String nazwa) {this.nazwa=nazwa;}
	//poniższa metoda posiada jako parametr wejściowy TABLICĘ String. Nie wiemy ile dokładnie elementów będzie
	//chciał przekazać do tablicy użytkownik; w związku z tym Java sama, na podstawie podanych wartości po przecinku
	//stworzy z nich tablicę i wrzuci jako zmienną do funkcji. 
	public void setWykonwanie(String... wykonanie) {for(String s: wykonanie) this.wykonanie+=s+"\n";}
	public void setOpis(String...opis) {for(String s:opis) this.opis=s+"\n";}
	public void setPoziom(int poziom) {
		this.poziom=getPoziom(poziom);
	}
	
	public void setPoziom(Trudnosc poziom) {this.poziom=poziom;}
	
	public void setCzas(int czas) {this.czas=czas;}
	
	//przykład metody pobiedająceh czas i przeliczającej go na minuty. Jeżeli będziemy mieli inna ilość elementów niż dwa (godziny
	//oraz minuty) to zapewne nie podaliśmy czasu - otrzymamy stosowny komunikat
	private static int getCzas(String czas) throws Exception {
		String[] c = czas.split(":");
		if (c.length!=2) throw new Exception("Podany ciąg nie zawiera dwukropka (jako separatora) lub zawiera ich zbyt wiele");
		int t = Integer.valueOf(c[1]) + 60 * Integer.valueOf(c[0]);
		return t;
	}
	
	public void setCzas(String czas) {
		try {
			this.czas = getCzas(czas);
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}
	
	//ponieważ pracujemy z listą, nie możemy w prosty sposób nią zarządzać. Na listę możemy dodawać nowe wartości,
	//możemy je z niej wyszukiwać oraz usuwać. W tym wypadku na liście będziemy mieli określone składniki niezbędne do wykonania przepisu
	//dla uproszczenia nie dodajemy na listę ilości wskazanego składnika (chociaż dobrze byłoby to dorobić)
	public List<Skladniki> getSkladniki() {
		return sklad; //zwrócenie listy WSZYSTKICH ZAPISANYCH SKŁADNIKÓW
	}
	
	public void dodajSkladnik(Skladniki sklad) {
		this.sklad.add(sklad); //dodanie nowego składnika do listy
	}
	
	//funkkcja pozwala na dodanie nowego składnika do listy tylko po nazwie (niejawnie - bez naszej wiedzy);
	//tworzy nowy obiekt z klasy Skladniki, dodaje do niego nazwę i dodaje ją do listy
	public void dodajSkladnik(String nazwa) {
		Skladniki s = new Skladniki();
		s.setNazwa(nazwa);
		dodajSkladnik(s);
	}
	
	//usuwanie składników polega na odpowiednim przeszukaniu listy; poniżej wyszukiwanie po nazwie (musimy przejrzeć wszystkie
	//zapisane na liście składniki i wyrzucić ten niepotrzebny)
	public void usunSkladnik(String nazwa) {
		for (Skladniki s: this.sklad) {
			if (s.getValue(Val.NAZWA) == nazwa) {
				usunSkladnik(s);
				break;
			}
		}
	}
	
	//tutaj usuwanie jest postsze - usuwamy cały obiekt; jeżeli szukamy na liście danego typu wartości tego samego typu
	// możemy, w przeciwieństwie do poprzedniej metody, wykorzystac wbudowaną metodę indexOf, zwracająca numer, pod którym 
	//znajduje się poszukiwany element; na koniec usuwamy go (jeżeli zwrócone ID jest większe od -1; -1 oznacza, że elementu nie ma liście)
	public void usunSkladnik(Skladniki sklad) {
		int ind = this.sklad.indexOf(sklad);
		if (ind > -1) this.sklad.remove(ind);
	}
	
	//NALEŻY!
	//dorobić funkcje podmieniania wartości, usuwająca duplikaty, czyszczącą całą listę oraz ładującą całą listę (z przekazanej 
	//parametrem listy)
}
