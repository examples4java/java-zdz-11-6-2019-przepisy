-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 11 Cze 2019, 16:48
-- Wersja serwera: 10.1.38-MariaDB
-- Wersja PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `przepisy_java`
--
CREATE DATABASE IF NOT EXISTS `przepisy_java` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `przepisy_java`;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `galerie`
--

DROP TABLE IF EXISTS `galerie`;
CREATE TABLE `galerie` (
  `id_galeria` bigint(20) UNSIGNED NOT NULL,
  `nazwa` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `galerie_przepis`
--

DROP TABLE IF EXISTS `galerie_przepis`;
CREATE TABLE `galerie_przepis` (
  `id_galeria` bigint(20) UNSIGNED NOT NULL,
  `id_przepis` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `galerie_zdjecia`
--

DROP TABLE IF EXISTS `galerie_zdjecia`;
CREATE TABLE `galerie_zdjecia` (
  `id_galeria` bigint(20) UNSIGNED NOT NULL,
  `id_zdjecie` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `przepisy`
--

DROP TABLE IF EXISTS `przepisy`;
CREATE TABLE `przepisy` (
  `id_przepis` bigint(20) UNSIGNED NOT NULL,
  `nazwa` varchar(60) NOT NULL,
  `trudnosc` tinyint(1) NOT NULL,
  `opis` text NOT NULL,
  `czas_wykonania` time NOT NULL,
  `opis_wykonania` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `skladniki`
--

DROP TABLE IF EXISTS `skladniki`;
CREATE TABLE `skladniki` (
  `id_skladnik` bigint(20) UNSIGNED NOT NULL,
  `nazwa` varchar(50) NOT NULL,
  `opis` text NOT NULL,
  `skad` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `skladniki_przepis`
--

DROP TABLE IF EXISTS `skladniki_przepis`;
CREATE TABLE `skladniki_przepis` (
  `id_skladnik` bigint(20) UNSIGNED NOT NULL,
  `id_przepis` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zdjecia`
--

DROP TABLE IF EXISTS `zdjecia`;
CREATE TABLE `zdjecia` (
  `id_zdjecie` bigint(20) UNSIGNED NOT NULL,
  `sciezka` varchar(100) NOT NULL,
  `opis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `galerie`
--
ALTER TABLE `galerie`
  ADD UNIQUE KEY `id_galeria` (`id_galeria`);

--
-- Indeksy dla tabeli `galerie_przepis`
--
ALTER TABLE `galerie_przepis`
  ADD KEY `id_galeria` (`id_galeria`),
  ADD KEY `id_przepis` (`id_przepis`);

--
-- Indeksy dla tabeli `galerie_zdjecia`
--
ALTER TABLE `galerie_zdjecia`
  ADD KEY `id_galeria` (`id_galeria`),
  ADD KEY `id_zdjecie` (`id_zdjecie`);

--
-- Indeksy dla tabeli `przepisy`
--
ALTER TABLE `przepisy`
  ADD UNIQUE KEY `id_przepis` (`id_przepis`);

--
-- Indeksy dla tabeli `skladniki`
--
ALTER TABLE `skladniki`
  ADD UNIQUE KEY `id_skladnik` (`id_skladnik`);

--
-- Indeksy dla tabeli `skladniki_przepis`
--
ALTER TABLE `skladniki_przepis`
  ADD KEY `id_skladnik` (`id_skladnik`),
  ADD KEY `id_przepis` (`id_przepis`);

--
-- Indeksy dla tabeli `zdjecia`
--
ALTER TABLE `zdjecia`
  ADD UNIQUE KEY `id_zdjecie` (`id_zdjecie`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `galerie`
--
ALTER TABLE `galerie`
  MODIFY `id_galeria` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `przepisy`
--
ALTER TABLE `przepisy`
  MODIFY `id_przepis` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `skladniki`
--
ALTER TABLE `skladniki`
  MODIFY `id_skladnik` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `zdjecia`
--
ALTER TABLE `zdjecia`
  MODIFY `id_zdjecie` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `galerie_przepis`
--
ALTER TABLE `galerie_przepis`
  ADD CONSTRAINT `galerie_przepis_ibfk_1` FOREIGN KEY (`id_galeria`) REFERENCES `galerie` (`id_galeria`),
  ADD CONSTRAINT `galerie_przepis_ibfk_2` FOREIGN KEY (`id_przepis`) REFERENCES `przepisy` (`id_przepis`);

--
-- Ograniczenia dla tabeli `galerie_zdjecia`
--
ALTER TABLE `galerie_zdjecia`
  ADD CONSTRAINT `galerie_zdjecia_ibfk_1` FOREIGN KEY (`id_galeria`) REFERENCES `galerie` (`id_galeria`),
  ADD CONSTRAINT `galerie_zdjecia_ibfk_2` FOREIGN KEY (`id_zdjecie`) REFERENCES `zdjecia` (`id_zdjecie`);

--
-- Ograniczenia dla tabeli `skladniki_przepis`
--
ALTER TABLE `skladniki_przepis`
  ADD CONSTRAINT `skladniki_przepis_ibfk_1` FOREIGN KEY (`id_przepis`) REFERENCES `przepisy` (`id_przepis`),
  ADD CONSTRAINT `skladniki_przepis_ibfk_2` FOREIGN KEY (`id_skladnik`) REFERENCES `skladniki` (`id_skladnik`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
