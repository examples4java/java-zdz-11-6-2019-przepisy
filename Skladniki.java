package Przepis;

public class Skladniki {
	String nazwa, opis;
	String pochodzenie;
	
	public void setNazwa(String nazwa) {
		this.nazwa=nazwa;
	}
	public void setOpis(String...opis) {
		for(String s:opis) this.opis+=s+"\n";
	}
	public void setPochodzenie(String...pochodzenie) {
		for(String s:pochodzenie) this.pochodzenie+=s+", ";
		this.pochodzenie=this.pochodzenie.substring(0, this.pochodzenie.length()-2);
	}
	
	public String getValue(Val v) {
		return getValue(v.get());
	}
	
	public String getValue(int v) {
		switch (v) {
		case 0:
			return nazwa;
		case 1:
			return opis;
		case 2:
			return pochodzenie;
		default:
			return "Zły parametr!";
		}
	}
	
	
	public static enum Val {
		NAZWA(0),
		OPIS(1),
		POCHODZENIE(2);
		
		private final int v;
		
		private Val(int v) {this.v=v;}
		
		public int get() {return v;}
	}
}
